# Machine class refactor

## Assumptions
* Because the only `type` values defined are 0 through 4, any other values are invalid
* Values returned by `getMaxSpeed()` method are correct (despite logic inversion)

## Major refactors
* Add constructor, with required int `machineType` parameter, and populate values for class in constructor (following logic from original implementation)
* Use string interpolcation for `description`
* Change `color` and `trimColor` to properties with private setters (setting values in constructor)
* Convert `isDark()` method to use switch statement
* Retain `getMaxSpeed()` method, but have it return private property which was set in constructor