using System;

namespace FortCodeExercises.Exercise1
{
    public class Machine
    {
        public Machine(int machineType)
        {
            type = machineType;
            switch (machineType)
            {
                case 0:
                    machineName = "bulldozer";
                    color = "red";
                    trimColor = "";
                    _maxSpeed = 80;
                    break;
                case 1:
                    machineName = "crane";
                    color = "blue";
                    trimColor = "white";
                    _maxSpeed = 75;
                    break;
                case 2:
                    machineName = "tractor";
                    color = "green";
                    trimColor = "gold";
                    _maxSpeed = 90;
                    break;
                case 3:
                    machineName = "truck";
                    color = "yellow";
                    trimColor = "";
                    _maxSpeed = 70;
                    break;
                case 4:
                    machineName = "car";
                    color = "brown";
                    trimColor = "";
                    _maxSpeed = 70;
                    break;
                default:
                    throw new Exception("Invalid machine type value");
            }
        }

        public int type = 0;

        public string machineName = "";

        public string name => machineName;

        public string description => $" {color} {name} [{getMaxSpeed()}].";

        public string color { get; private set; }

        public string trimColor { get; private set; }

        public bool isDark(string color)
        {
            switch (color)
            {
                case "red":
                case "green":
                case "black":
                case "crimson":
                    return true;
                default:
                    return false;
            }
        }

        private int _maxSpeed;
        public int getMaxSpeed() => _maxSpeed;
    }
}